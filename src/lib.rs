#[cfg(test)]
mod tests {
    use super::Uval;
    #[test]
    fn it_works() {
        test(Uval::from_14_digit_decimal(/*0.*/ 00000085100000), 1298888290);
    }

    #[test]
    fn vs_float() {
        assert_eq!(Uval::from_14_digit_decimal(/*0.*/ 00000085100000) * 1298888290, 1106);
        assert!(((0.16222612 * 1298888290.0) - 1106.0) > 0.5);
    }

    fn test(uval: Uval, int: u32) {
        let res = uval * int;
        eprintln!("0.{:032b} * {:b} = {:b}", uval.0, int, res);
        eprintln!("{:.64} * {} = {}", uval, int, res);
    }
}

use std::ops::{Add, AddAssign, Mul, MulAssign, Sub, SubAssign};

#[derive(Copy, Clone)]
pub struct Uval(u32);

impl Uval {
    pub fn from_14_digit_decimal(decimal: u64) -> Self {
        if decimal == 0 {
            return Uval(0);
        }
        let mut nom = decimal;
        let denom = 10u64.pow(14);
        let mut y = 0;
        for b in 0..32 {
            if nom >= denom {
                nom = nom - denom;
                y |= 1 << 32 - b;
                if nom == 0 {
                    break;
                }
            }
            nom = nom * 2;
        }
        Uval(y)
    }
}

impl Mul<u32> for Uval {
    type Output = u32;

    fn mul(self, rhs: u32) -> Self::Output {
        let x = self.0 as u64 * rhs as u64;
        if x & (1 << 32) != 0 {
            ((x >> 32) + 1) as u32
        } else {
            (x >> 32) as u32
        }
    }
}

impl Mul<Uval> for u32 {
    type Output = u32;

    fn mul(self, rhs: Uval) -> Self::Output {
        let x = rhs.0 as u64 * self as u64;
        if x & (1 << 32) != 0 {
            ((x >> 32) + 1) as u32
        } else {
            (x >> 32) as u32
        }
    }
}

impl MulAssign<Uval> for u32 {
    fn mul_assign(&mut self, rhs: Uval) {
        *self = *self * rhs;
    }
}

impl Add for Uval {
    type Output = Uval;

    fn add(self, rhs: Self) -> Self::Output {
        Uval(self.0.saturating_add(rhs.0))
    }
}

impl AddAssign for Uval {
    fn add_assign(&mut self, rhs: Self) {
        self.0 = self.0.saturating_add(rhs.0);
    }
}

use std::fmt;
impl fmt::Display for Uval {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let accuracy = f.precision().unwrap_or(8);
        let mut y = Vec::with_capacity(accuracy);
        if self.0 == 0 {
            y.push('0');
        } else {
            let mut nom = self.0 as u64;
            let denom = 1u64 << 32;

            for _ in 0..accuracy {
                if nom == 0 {
                    break;
                }
                nom = nom * 10;
                if nom >= denom {
                    let fraction = nom / denom;
                    y.push((fraction as u8 + 48u8) as char);
                    nom = nom % denom;
                    if nom == 0 {
                        break;
                    }
                } else {
                    y.push('0');
                }
            }
        }
        write!(f, "0.{}", y.into_iter().collect::<String>())
    }
}

impl Sub for Uval {
    type Output = Uval;

    fn sub(self, rhs: Self) -> Self::Output {
        Uval(self.0.saturating_sub(rhs.0))
    }
}

impl SubAssign for Uval {
    fn sub_assign(&mut self, rhs: Self) {
        self.0 = self.0.saturating_sub(rhs.0);
    }
}
